import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// react components for routing our app without refresh
// import { Link } from "react-router-dom";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons
// core components
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
// import Button from "components/CustomButtons/Button.js";
import Parallax from "components/Parallax/Parallax.js";
import NavPills from "components/NavPills/NavPills.js";
import Chart from 'react-google-charts';
// sections for this page
import HeaderLinks from "components/Header/HeaderLinks.js";
import SectionPills from "./Sections/SectionPills.js";
// import SectionBasics from "./Sections/SectionBasics.js";
// import SectionNavbars from "./Sections/SectionNavbars.js";
// import SectionTabs from "./Sections/SectionTabs.js";
// import SectionNotifications from "./Sections/SectionNotifications.js";
// import SectionTypography from "./Sections/SectionTypography.js";
// import SectionJavascript from "./Sections/SectionJavascript.js";
// import SectionCarousel from "./Sections/SectionCarousel.js";
// import SectionCompletedExamples from "./Sections/SectionCompletedExamples.js";
// import SectionLogin from "./Sections/SectionLogin.js";
// import SectionExamples from "./Sections/SectionExamples.js";
// import SectionDownload from "./Sections/SectionDownload.js";

import styles from "assets/jss/material-kit-react/views/components.js";
import { Progress,Card, Col, Row,BackTop } from 'antd';

const style = {
  height: 40,
  width: 40,
  lineHeight: '40px',
  borderRadius: 4,
  backgroundColor: '#1088e9',
  color: '#fff',
  textAlign: 'center',
  fontSize: 14,
};
const useStyles = makeStyles(styles);

export default function Components(props) {
  const classes = useStyles();
  const { ...rest } = props;
  return (
    <div>
    
      <Header
        brand={<img
                              alt="..."
                              src="https://filantropi.or.id/wp-content/uploads/2020/01/fi-logo-2.png"
                              width="150"
                              //className={navImageClasses}
                            />}
        rightLinks={<HeaderLinks />}
        fixed
        color="transparent"
        changeColorOnScroll={{
          height: 400,
          color: "white"
        }}
        {...rest}
      />
      {/* <Parallax  style={{height:'85vh'}} image={require("assets/img/virus1.jpg")}> */}
      <Parallax style={{height:'85vh'}}  className={classes.covers} >
        <div className={classes.container}>
          <GridContainer>
            {/* <GridItem xs={12} sm={6} md={7}>
                
            </GridItem> */}
            

            <GridContainer>
            <GridItem xs={12} sm={12} md={8} lg={6}>
            <div className={classes.brand}>
                    <h1 className={classes.title}>Covid-19</h1>
                    <h3 className={classes.subtitle}>DARURAT CORONAVIRUS</h3>
                    <h3 className={classes.subtitle}>Tanggapan filantropi terhadap COVID-19</h3>
                    <h3 className={classes.subtitle}>Perusahaan, yayasan dan individu mana yang mendukung keadaan darurat coronavirus dan bagaimana mereka mengalokasikan dana?</h3>
                </div>
             
            </GridItem>
            <GridItem xs={12} sm={12} md={12} lg={6}>
            <div >
            <Row gutter={16}>
              <Col span={12}>
                <Card title="Global" style={{ width: 300 }}>
               <div style={{display:'flex',justifyContent:'space-between',marginTop:-20}}> 
                  
                    <h5 style={{fontWeight:'bold'}}>Negara/Kawasan</h5>
                  
                  
                    <h5>212</h5>
                  
               </div>
               <div style={{display:'flex',justifyContent:'space-between',}}> 
                  
                  <h5 style={{fontWeight:'bold'}}>Kasus Terkonfirmasi</h5>
                
                
                  <h5>1,356,780</h5>
                
             </div>
             <div style={{display:'flex',justifyContent:'space-between',}}> 
                  
                  <h5 style={{fontWeight:'bold'}}>Kematian</h5>
                
                
                  <h5 >79,385</h5>
                
             </div>
                </Card>
              </Col>
              <Col span={12}>
                <Card title="Indonesia" style={{ width: 300 }}>
                <div style={{display:'flex',justifyContent:'space-between',marginTop:-20}}> 
                  
                    <h5 style={{fontWeight:'bold', color:'gold'}}>Positif</h5>
                  
                  
                    <h5>3,293</h5>
                  
               </div>
               <div style={{display:'flex',justifyContent:'space-between'}}> 
                  
                  <h5 style={{fontWeight:'bold', color:'green'}}>Sembuh</h5>
                
                
                  <h5>252</h5>
                
             </div>
             <div style={{display:'flex',justifyContent:'space-between'}}> 
                  
                  <h5 style={{fontWeight:'bold', color:'red'}} >Meninggal</h5>
                
                
                  <h5>280</h5>
                
             </div>
                </Card>
              </Col>
              
            </Row>
            <Card style={{ width: 586 ,height:250}}>
            <Chart
              width={'500px'}
              height={'200px'}
              chartType="GeoChart"
              data={[
                ['Country', 'Popularity'],
                ['Germany', 200],
                ['United States', 300],
                ['Brazil', 400],
                ['Canada', 500],
                ['France', 600],
                ['RU', 700],
              ]}
              // Note: you will need to get a mapsApiKey for your project.
              // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
              mapsApiKey="YOUR_KEY_HERE"
              rootProps={{ 'data-testid': '1' }}
            />
                </Card>
          </div>
            {/* <NavPills
                color="primary"
                tabs={[
                  {tabButton: "Semua",
                    //tabIcon: Dashboard,
                    tabContent: (
                      <div style={{ height: 200 }}>
                         <p>
                         
                          Yayasan
                        </p>
                        <Progress percent={50} status="active" />
                        <p>
                         
                      Perusahaan
                       </p>
                       <Progress percent={50} status="active" />
                       <p>
                         
                         Organisasi Nirlaba
                       </p>
                       <Progress percent={50} status="active" />
                       <p>
                         
                         Swasta
                       </p>
                       <Progress percent={50} status="active" />
                       
                        
                      </div>
                    )
                  },
                  {tabButton: "Sumbangan Barang",
                    //tabIcon: Schedule,
                    tabContent: (
                      <span>
                        <p>
                          Efficiently unleash cross-media information without
                          cross-media value. Quickly maximize timely
                          deliverables for real-time schemas.
                        </p>
                        <br />
                        <p>
                          Dramatically maintain clicks-and-mortar solutions
                          without functional solutions. Dramatically visualize
                          customer directed convergence without revolutionary
                          ROI. Collaboratively administrate empowered markets
                          via plug-and-play networks. Dynamically procrastinate
                          B2C users after installed base benefits.
                        </p>
                      </span>
                    )
                  },
                  {tabButton: "Sumbangan Uang",
                    //tabIcon: List,
                    tabContent: (
                      <span>
                        <p>
                          Collaboratively administrate empowered markets via
                          plug-and-play networks. Dynamically procrastinate B2C
                          users after installed base benefits.
                        </p>
                        <br />
                        <p>
                          Dramatically visualize customer directed convergence
                          without revolutionary ROI. Collaboratively
                          administrate empowered markets via plug-and-play
                          networks. Dynamically procrastinate B2C users after
                          installed base benefits.
                        </p>
                        <br />
                        <p>
                          Dramatically visualize customer directed convergence
                          without revolutionary ROI. Collaboratively
                          administrate empowered markets via plug-and-play
                          networks. Dynamically procrastinate B2C users after
                          installed base benefits.
                        </p>
                      </span>
                    )
                  },
                  {tabButton: "Donasi Layanan",
                    //tabIcon: List,
                    tabContent: (
                      <span>
                        <p>
                          Collaboratively administrate empowered markets via
                          plug-and-play networks. Dynamically procrastinate B2C
                          users after installed base benefits.
                        </p>
                        <br />
                        <p>
                          Dramatically visualize customer directed convergence
                          without revolutionary ROI. Collaboratively
                          administrate empowered markets via plug-and-play
                          networks. Dynamically procrastinate B2C users after
                          installed base benefits.
                        </p>
                        <br />
                        <p>
                          Dramatically visualize customer directed convergence
                          without revolutionary ROI. Collaboratively
                          administrate empowered markets via plug-and-play
                          networks. Dynamically procrastinate B2C users after
                          installed base benefits.
                        </p>
                      </span>
                    )
                  },
                ]}
              /> */}
            </GridItem>
          </GridContainer>
          </GridContainer>
        </div>
      </Parallax>

      <div className={classNames(classes.main, classes.mainRaised)}>
        {/* <SectionBasics /> */}
        {/* <SectionNavbars /> */}
        {/* <SectionTabs /> */}
        <SectionPills />
        {/* <SectionNotifications />
        <SectionTypography />
        <SectionJavascript /> */}
        {/* <SectionCarousel /> */}
        {/* <SectionCompletedExamples /> */}
        {/* <SectionLogin /> */}
        {/* <GridItem md={12} className={classes.textCenter}>
          <Link to={"/login-page"} className={classes.link}>
            <Button color="primary" size="lg" simple>
              View Login Page
            </Button>
          </Link>
        </GridItem> */}
        {/* <SectionExamples />
        <SectionDownload /> */}
      </div>

      <Footer />
      <BackTop>
      <div style={style}>UP</div>
    </BackTop>
    </div>
  );
}
