import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import classNames from "classnames";

// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Schedule from "@material-ui/icons/Schedule";
import List from "@material-ui/icons/List";
import { Card, Col, Row ,Avatar,Carousel,Select} from 'antd';
// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import NavPills from "components/NavPills/NavPills.js";
import Chat from "@material-ui/icons/Chat";
import VerifiedUser from "@material-ui/icons/VerifiedUser";
import Fingerprint from "@material-ui/icons/Fingerprint";
// core components
import InfoArea from "components/InfoArea/InfoArea.js";
import styles from "assets/jss/material-kit-react/views/componentsSections/pillsStyle.js";
import { EditOutlined, EllipsisOutlined, SettingOutlined } from '@ant-design/icons';


const useStyles = makeStyles(styles);
const { Meta } = Card;
const { Option } = Select;

const children = [];
for (let i = 10; i < 36; i++) {
  children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
}

function handleChange(value) {
  console.log(`selected ${value}`);
}

export default function SectionPills(props) {
  const classes = useStyles();
  const { ...rest } = props;
  const imageClasses = classNames(
    classes.imgRaised,
    classes.imgRoundedCircle,
    classes.imgFluid
  );
  return (
    <div className={classes.section}>
      <div className={classes.container}>
      <GridContainer justify="center">
              <GridItem xs={12} sm={6} md={3}>
                <div className={classes.profile}>
                  <div style={{width:50,height:50,background:'white'}}>
                  <h6></h6>
                  </div>
                  <div className={classes.name}>
                    <h3 className={classes.title}>123456</h3>
                    <h6>DESIGNER</h6>
                    
                  </div>
                </div>
              </GridItem>
              <GridItem xs={12} sm={6} md={3}>
                <div className={classes.profile}>
                 <div style={{width:50,height:50,background:'white'}}>
                  <h6></h6>
                  </div>
                  <div className={classes.name}>
                    <h3 className={classes.title}>123456</h3>
                    <h6>Total sumbangan dan dana</h6>
                    
                  </div>
                </div>
              </GridItem>
              <GridItem xs={12} sm={6} md={3}>
                <div className={classes.profile}>
                 <div style={{width:50,height:50,background:'white'}}>
                  <h6></h6>
                  </div>
                  <div className={classes.name}>
                    <h3 className={classes.title}>123456</h3>
                    <h6>Jumlah donor / promotor</h6>
                    
                  </div>
                </div>
              </GridItem>
              <GridItem xs={12} sm={6} md={3}>
                <div className={classes.profile}>
                <div style={{width:50,height:50,background:'whites'}}>
                  <h6></h6>
                  </div>
                  <div className={classes.name}>
                    <h3 className={classes.title}>123456</h3>
                    <h6>DESIGNER</h6>
                    
                  </div>
                </div>
              </GridItem>
            </GridContainer>
        <div id="navigation-pills">
          
         <hr/>
        <div className={classes.title}>
            <h3>Navigation Pills</h3>
          </div>
          <GridContainer>
          <GridItem>
              <NavPills
                color="rose"
                horizontal={{
                  tabsGrid: { xs: 12, sm: 4, md: 4 },
                  contentGrid: { xs: 12, sm: 8, md: 8 }
                }}
                tabs={[
                  {
                    tabButton: "Mengapa",
                    //tabIcon: Dashboard,
                    tabContent: (
                      <span>
                        <p>
                          Epidemi Coronavirus menyebabkan kesulitan bagi perekonomian dan dalam waktu singkat akan memaksa warga untuk mengubah kebiasaan yang dianggap terkonsolidasi dan tidak dapat diubah. Sektor Ketiga , seperti rumah tangga dan bisnis, juga menderita akibat ekonomi dari epidemi, dan harus menghadapi keberlanjutan kegiatannya .
                        </p>
                        <br />
                        {/* <p>
                          Dramatically visualize customer directed convergence
                          without revolutionary ROI. Collaboratively
                          administrate empowered markets via plug-and-play
                          networks. Dynamically procrastinate B2C users after
                          installed base benefits.
                        </p>
                        <br />
                        <p>
                          Dramatically visualize customer directed convergence
                          without revolutionary ROI. Collaboratively
                          administrate empowered markets via plug-and-play
                          networks. Dynamically procrastinate B2C users after
                          installed base benefits.
                        </p> */}
                      </span>
                    )
                  },
                  {
                    tabButton: "Tujuan",
                    //tabIcon: Schedule,
                    tabContent: (
                      <span>
                        <p>
                          Tingkatkan dampak respons darurat melalui tiga tindakan berikut:
                         
                          
                          
                        </p>
                       
                        <p>
                        1. Sekilas tentang mendukung inisiatif berlangsung, selalu dapat diakses
                        
                        </p>
                       
                        <p>
                        2. Pemetaan kebutuhan melalui pengumpulan dan pertukaran informasi
                        
                        </p>
                       
                        <p>
                        3. Berbagi praktik, strategi, dan hasil terbaik untuk membuat model intervensi yang dapat ditiru dan mengurangi pemborosan
                        
                        </p>
                      </span>
                    )
                  },
                  {
                    tabButton: "Promotor",
                    //tabIcon: List,
                    tabContent: (
                      <span>
                        <p>
                        Realitas yang berbeda di garis depan untuk desain inisiatif filantropi terkoordinasi:
                        </p>
                      
                        <p>
                        Yayasan asal perbankan, yayasan komunitas, yayasan bisnis, yayasan keluarga, perusahaan, organisasi nirlaba dan jaringan kategori.
                        </p>
                      </span>
                    )
                  }
                ]}
              />
            </GridItem>
          </GridContainer>
          <GridContainer>
          <GridItem>
          <div>
          <div className={classes.title}>
            <h3>Kategorisasi bantuan</h3>
          </div>
          
         
          <Select
    mode="multiple"
    style={{ width: '100%' }}
    placeholder="Please select"
    defaultValue={['a10', 'c12']}
    onChange={handleChange}
    size={"large"}
  >
    {children}
  </Select> </div>
            </GridItem>
          </GridContainer>
          <div className={classes.title}>
            <h3>Berita</h3>
          </div>
          {/* <Carousel autoplay>
            <div>
            <img
                alt="example"
                src="https://www.covid19.go.id/wp-content/uploads/2020/04/Achmad-Yurianto-22.jpeg"
              />
            </div>
            <div>
              <h3>2</h3>
            </div>
            <div>
              <h3>3</h3>
            </div>
            <div>
              <h3>4</h3>
            </div>
          </Carousel> */}
          <div  className={classes.sitecardwrapper} >
            <Row gutter={[16, 8]}>
              <Col span={8}>
                <Card
            hoverable
             style={{ width: 350 }}
            cover={
              <img
                alt="example"
                src="https://www.covid19.go.id/wp-content/uploads/2020/04/Achmad-Yurianto-22.jpeg"
              />
            }
            // actions={[
            //   <SettingOutlined key="setting" />,
            //   <EditOutlined key="edit" />,
            //   <EllipsisOutlined key="ellipsis" />,
            // ]}
          >
            <Meta
              avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
              title="Card title"
              description="This is the description"
            />
          </Card>
              </Col>
              <Col span={8}>
              <Card
            hoverable
             style={{ width: 350 }}
            cover={
              <img
                alt="example"
                src="https://www.covid19.go.id/wp-content/uploads/2020/04/Achmad-Yurianto-22.jpeg"
              />
            }
            // actions={[
            //   <SettingOutlined key="setting" />,
            //   <EditOutlined key="edit" />,
            //   <EllipsisOutlined key="ellipsis" />,
            // ]}
          >
            <Meta
              avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
              title="Card title"
              description="This is the description"
            />
          </Card>
              </Col>
              <Col span={8}>
                 <Card
            hoverable
             style={{ width: 350 }}
            cover={
              <img
                alt="example"
                src="https://www.covid19.go.id/wp-content/uploads/2020/04/Achmad-Yurianto-24.jpeg"
              />
            }
            // actions={[
            //   <SettingOutlined key="setting" />,
            //   <EditOutlined key="edit" />,
            //   <EllipsisOutlined key="ellipsis" />,
            // ]}
          >
            <Meta
              
              title="Card title"
              description="This is the description"
            />
          </Card>
              </Col>
              
            </Row>
            
          </div>
          
        </div>
      </div>
    </div>
  );
}
